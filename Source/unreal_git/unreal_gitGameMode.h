// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "unreal_gitGameMode.generated.h"

UCLASS(minimalapi)
class Aunreal_gitGameMode : public AGameModeBase
{
	GENERATED_BODY()

public:
	Aunreal_gitGameMode();
};



