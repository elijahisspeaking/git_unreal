// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once 

#include "CoreMinimal.h"
#include "GameFramework/HUD.h"
#include "unreal_gitHUD.generated.h"

UCLASS()
class Aunreal_gitHUD : public AHUD
{
	GENERATED_BODY()

public:
	Aunreal_gitHUD();

	/** Primary draw call for the HUD */
	virtual void DrawHUD() override;

private:
	/** Crosshair asset pointer */
	class UTexture2D* CrosshairTex;

};

