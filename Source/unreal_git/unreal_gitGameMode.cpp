// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "unreal_gitGameMode.h"
#include "unreal_gitHUD.h"
#include "unreal_gitCharacter.h"
#include "UObject/ConstructorHelpers.h"

Aunreal_gitGameMode::Aunreal_gitGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = Aunreal_gitHUD::StaticClass();
}
